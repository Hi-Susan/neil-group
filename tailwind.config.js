
module.exports = {
  content: ["./src/*.pug"],
  theme: {
    container: {
      center: true,
    },
    fontFamily: {
      sans: ['Nunito', 'Noto Sans TC', 'sans-serif'] // 改預設字體
    },
    colors: {
      main: {
        DEFAULT: '#214883',
        dark: '#0b2e4c'
      },
      subMain: '#E8C691',
      white: '#FFF9F0',
      rose: 'rgb(225 29 72)'
    },
    extend: {},
  },
  plugins: [],
}
